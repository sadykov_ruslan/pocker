import React, {Component} from 'react';
import Card from "./Card/Card";
import CardDeck from './Deck/Deck.js';
import './App.css';

class App extends Component {
    constructor() {
        super();
        this.state = {
            display: false,
            cards: CardDeck.getCards(5),
        }
    }
    displayDeck = () => {
        this.setState({display: true});
    }
    render() {
        return (
            <div>
                <button className='btn' onClick={this.displayDeck}>Button</button>
                {this.state.display ?
                    <div className="App playingCards fourColours faceImages">
                        <Card rank={this.cards[0].rank} suit={this.cards[0].suit} />
                        <Card rank={this.state.cards[1].rank} suit={this.state.cards[1].suit} />
                        <Card rank={this.state.cards[2].rank} suit={this.state.cards[2].suit} />
                        <Card rank={this.state.cards[3].rank} suit={this.state.cards[3].suit} />
                        <Card rank={this.state.cards[4].rank} suit={this.state.cards[4].suit} />
                    </div> :
                    null
                }
            </div>
        );
    }
}

export default App;

