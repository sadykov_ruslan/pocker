

let getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


class CardDeck {
    constructor () {
        this.deck = this.getDeck();

    }
    getDeck = () => {
        const suits = ['hearts', 'diamonds', 'clubs', 'spades'];
        const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k', 'a'];
        let currentDeck = [];
        for (let suit of suits) {
            for (let rank of ranks) {
                let card = {suit: suit, rank: rank};
                currentDeck.push(card);
            }
        }
        return currentDeck;
    }
    getCard = () => {
        let min = 0;
        let max = 52;
        let card;
        while (card === undefined) {
            let number = getRandomInt(min, max);
            card = this.deck[number];
            this.deck.splice(number, 1);
        }
        return card;
    }
    getCards = (props) => {
        let cardsArray = [];
        for (let i=0; i < props; i++) {
            let card = this.getCard();
            cardsArray.push(card);
        }
        return cardsArray;
    }

}

export default CardDeck;
