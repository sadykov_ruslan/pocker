import React, {Component} from 'react';

class Card extends Component {
    render(props) {
        const suits = {
            hearts: '♥',
            diamonds: '♦',
            clubs: '♣',
            spades: '♠'
        };
        const ranks = {
            q: 'Q',
            j: 'J',
            k: 'K',
            a: 'A',
            2: '2',
            3: '3',
            4: '4',
            5: '5',
            6: '6',
            7: '7',
            8: '8',
            9: '9',
            10: '10'
        }
        const  classes =this.props.rank + this.props.suit;
        return (
            <div className={classes}>
                <span className="rank">{ranks[this.props.rank]}</span>
                <span className="suit">{suits[this.props.suit]}</span>
            </div>);
    }
}

export default Card;